﻿using System.IO;
using System.Xml.Serialization;

namespace DataVersionControlExample
{
    public static class HelperExtensions
    {
        public static string Serialize(this object obj)
        {
            using (var writer = new StringWriter())
            {
                var serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(writer, obj);

                return writer.ToString();
            }
        }

        public static T Deserialize<T>(this string value)
        {
            using (var reader = new StringReader(value))
            {
                var serializer = new XmlSerializer(typeof(T));
                var obj = serializer.Deserialize(reader);

                return (T)obj;
            }
        }
    }
}
