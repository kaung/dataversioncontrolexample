﻿using System;

namespace DataVersionControlExample
{
    public class Person
    {
        private const int CURRENT_VERSION = 3;

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string GivenName { get; set; }
        public string SurName { get; set; }
        public string FullName { get; set; }
        public int Version { get; set; }

        public static Person Create(string firstName, string lastName)
        {
            return new Person
            {
                GivenName = firstName,
                SurName = lastName,
                FullName = String.Format("{0} {1}", firstName, lastName),
                Version = CURRENT_VERSION
            };
        }
    }
}
