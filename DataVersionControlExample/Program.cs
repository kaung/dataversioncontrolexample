﻿using System;
using System.IO;
using DataVersionControlExample.Versioning;

namespace DataVersionControlExample
{
    class Program
    {
        static void Main()
        {
            // Change the version number to see how each versioned data gets updated
            var filePath = Path.Combine(Environment.CurrentDirectory, "data\\personv2.xml");

            var data = File.ReadAllText(filePath);
            var person = data.Deserialize<Person>();

            var versionManager = new VersionManager();
            versionManager.VersionData(person);

            Console.WriteLine("My name is {0}.", person.FullName);

            Console.ReadKey();
        }
    }
}
