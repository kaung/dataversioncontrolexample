﻿using System.Globalization;

namespace DataVersionControlExample.Versioning.PersonVersioning
{
    public class PersonVersionHandler1 : PersonVersionHandlerBase
    {
        public PersonVersionHandler1(VersionHandlerBase nextHandler) : base(nextHandler)
        {
        }

        public override int Version
        {
            get { return 1; }
        }

        /// <summary>
        /// Fix name capitalization
        /// </summary>
        /// <param name="person"></param>
        public override void VersionData(Person person)
        {
            var textInfo = new CultureInfo("en-US", false).TextInfo;
            person.FirstName = textInfo.ToTitleCase(person.FirstName);
            person.LastName = textInfo.ToTitleCase(person.LastName);
        }
    }
}
