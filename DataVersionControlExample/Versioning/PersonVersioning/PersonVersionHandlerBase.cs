﻿using System;

namespace DataVersionControlExample.Versioning.PersonVersioning
{
    /// <summary>
    /// Person version handler base class that contains logic to check
    /// if the object for the request could be handled and to pass the
    /// request to the next handler in chain after it is handled. All
    /// person version handlers will derive from this.
    /// </summary>
    public abstract class PersonVersionHandlerBase : VersionHandlerBase
    {
        /// <summary>
        /// Instantiates a person version handler and takes in the next handler in chain
        /// </summary>
        /// <param name="nextHandler"></param>
        protected PersonVersionHandlerBase(VersionHandlerBase nextHandler)
        {
            this.NextHandler = nextHandler;
        }

        /// <summary>
        /// Gets the current version of the version handler. The handler needs
        /// to have the same or newer version than the version of Person obj to
        /// handle versioning.
        /// </summary>
        public abstract int Version { get; }

        /// <summary>
        /// Handle any versioning logic here.
        /// </summary>
        /// <param name="person">Person obj to version</param>
        public abstract void VersionData(Person person);

        /// <summary>
        /// Gets the next version handler in the chain to execute. This
        /// does not have to be PersonVersionHandlerBase type.
        /// </summary>
        public VersionHandlerBase NextHandler { get; private set; }

        /// <summary>
        /// Validates if obj is a Person and if handler version is newer
        /// than Person version. If so, version data.
        /// </summary>
        /// <param name="obj">Object to validate</param>
        /// <returns>Current handler can handle request</returns>
        protected virtual bool CanHandle(object obj)
        {
            var person = obj as Person;
            if (person != null)
                return this.Version > person.Version;

            return false;
        }

        /// <summary>
        /// Checks if handler could handle the object for the request.
        /// If so, VersionData. If not, pass it to the next handler in
        /// chain if it exists.
        /// </summary>
        /// <param name="obj">Object to version</param>
        public override void Handle(object obj)
        {
            if (CanHandle(obj))
            {
                Console.WriteLine("Versioning Person v{0}", this.Version);

                Person person = (Person)obj;
                VersionData(person);

                // Update the object's version.  This will make sure that
                // the object version is updated if it needs to be persisted
                // back to the data store.
                person.Version = this.Version;
            }

            if (this.NextHandler != null)
                this.NextHandler.Handle(obj);
        }
    }
}
