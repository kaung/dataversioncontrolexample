﻿using System;

namespace DataVersionControlExample.Versioning.PersonVersioning
{
    public class PersonVersionHandler3 : PersonVersionHandlerBase
    {
        public PersonVersionHandler3(VersionHandlerBase nextHandler) : base(nextHandler)
        {
        }

        public override int Version
        {
            get { return 3; }
        }

        /// <summary>
        /// Fix FullName
        /// </summary>
        /// <param name="person"></param>
        public override void VersionData(Person person)
        {
            person.FullName = String.Format("{0} {1}", person.GivenName, person.SurName);
        }
    }
}
