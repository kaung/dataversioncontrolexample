﻿namespace DataVersionControlExample.Versioning.PersonVersioning
{
    public class PersonVersionHandler2 : PersonVersionHandlerBase
    {
        public PersonVersionHandler2(VersionHandlerBase nextHandler) : base(nextHandler)
        {
        }

        public override int Version
        {
            get { return 2; }
        }

        /// <summary>
        /// Copy first/last name to claims based properties
        /// </summary>
        /// <param name="person"></param>
        public override void VersionData(Person person)
        {
            person.GivenName = person.FirstName;
            person.SurName = person.LastName;
        }
    }
}
