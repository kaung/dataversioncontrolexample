﻿using DataVersionControlExample.Versioning.PersonVersioning;

namespace DataVersionControlExample.Versioning
{
    public class VersionManager
    {
        private readonly VersionHandlerBase _handler;

        public VersionManager()
        {
            _handler = new PersonVersionHandler1(
                       new PersonVersionHandler2(
                       new PersonVersionHandler3(null)));
        }

        public void VersionData(object obj)
        {
            _handler.Handle(obj);
        }
    }
}
