﻿namespace DataVersionControlExample.Versioning
{
    public abstract class VersionHandlerBase
    {
        public abstract void Handle(object obj);
    }
}
